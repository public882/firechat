//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';

// create a component
const App = () => {
  return (
    <View style={styles.container}>
      <GiftedChat />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default App;
