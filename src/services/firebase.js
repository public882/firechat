import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export const currentUser = () => auth().currentUser.toJSON();

export const createNewThread = to => {
  const user = currentUser();
  const name = makeid();
  return firestore()
    .collection('MESSAGE_THREADS')
    .add({
      sender: user.uid,
      reciever: to.uid,
      message: 0,
      name: name,
      latestMessage: {
        text: `${name} created.`,
        createdAt: new Date().getTime(),
      },
    });
};

export const listenToThreads = () => {
  return firestore()
    .collection('MESSAGE_THREADS')
    .orderBy('latestMessage.createdAt', 'desc');
};

export const listenToMessages = threadId => {
  return firestore()
    .collection('MESSAGE_THREADS')
    .doc(threadId)
    .collection('MESSAGES')
    .orderBy('createdAt', 'desc');
};

export const createMessage = async (threadId, text, to) => {
  const user = currentUser();
  const increment = firestore.FieldValue.increment(1);
  return firestore()
    .collection('MESSAGE_THREADS')
    .doc(threadId)
    .set(
      {
        latestMessage: {
          text,
          createdAt: new Date().getTime(),
        },
        message: increment,
      },
      {merge: true},
    )
    .then(() => {
      return firestore()
        .collection('MESSAGE_THREADS')
        .doc(threadId)
        .collection('MESSAGES')
        .add({
          text,
          createdAt: new Date().getTime(),
          user: {
            _id: user.uid,
            name: user.displayName,
          },
        });
    });
};

export const markThreadLastRead = (user, threadId, time) => {
  return firestore()
    .collection('USER_THREAD_TRACK')
    .doc(user.uid)
    .set(
      {
        [threadId]: time,
      },
      {merge: true},
    );
};

export const listenToThreadTracking = () => {
  const user = currentUser();
  return firestore().collection('USER_THREAD_TRACK').doc(user.uid);
};

export const signUpUser = (email, password) => {
  return auth().createUserWithEmailAndPassword(email, password);
};
export const loginUser = (email, password) => {
  return auth().signInWithEmailAndPassword(email, password);
};

export const createUser = data => {
  return firestore().collection('USERS').doc(data.uid).set(
    {
      email: data.email,
      displayName: data.displayName,
      uid: data.uid,
      createdAt: new Date().getTime(),
    },
    {merge: true},
  );
};

export const allUser = () => {
  return firestore().collection('USERS').orderBy('email', 'asc');
};

export const makeid = (length = 10) => {
  var result = '';
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const getThreadUser = async to => {
  const user = currentUser();
  const thread1 = firestore()
    .collection('MESSAGE_THREADS')
    .where('sender', '==', to.uid)
    .where('reciever', '==', user.uid)
    .get();
  const thread2 = firestore()
    .collection('MESSAGE_THREADS')
    .where('sender', '==', user.uid)
    .where('reciever', '==', to.uid)
    .get();

  const [queryThread1, queryThread2] = await Promise.all([thread1, thread2]);
  const queryThread1Array = queryThread1.docs;
  const queryThread2Array = queryThread2.docs;
  return queryThread1Array.concat(queryThread2Array);
};
