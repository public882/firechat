//import liraries
import React, {useState, useEffect} from 'react';
import database from '@react-native-firebase/database';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  listenToThreads,
  listenToThreadTracking,
  createNewThread,
  currentUser,
} from '../services/firebase';

// create a component

const isThreadUnread = (thread, threadTracking) => {
  if (
    threadTracking[thread._id] &&
    threadTracking[thread._id] < thread.latestMessage.createdAt
  ) {
    return true;
  }

  return false;
};

const Threads = props => {
  const user = currentUser();
  const {navigation} = props;
  const [threads, setThreads] = useState([]);
  const [threadTracking, setThreadTracking] = useState({});

  useEffect(() => {
    // loadData();
    const unsubscribe = listenToThreads()?.onSnapshot(querySnapshot => {
      const allThreads = querySnapshot?.docs
        ?.filter(
          item =>
            (item.data().sender === user.uid ||
              item.data().reciever === user.uid) &&
            item.data().message > 0,
        )
        .map(snapshot => {
          console.log(snapshot.data());
          return {
            _id: snapshot?.id,
            name: '',
            latestMessage: {text: ''},
            ...snapshot?.data(),
          };
        });
      setThreads(allThreads);
    });
    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    const unsubscribe = listenToThreadTracking().onSnapshot(snapshot => {
      setThreadTracking(snapshot.data() || {});
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    getUser();
  }, []);
  const getUser = async () => {
    database()
      .ref('users')
      // .once('value')
      .on('value', data => console.log('DATABASE', data));
    // .then();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Users')}
        style={{alignItems: 'center'}}>
        <Text>Users</Text>
      </TouchableOpacity>
      <ScrollView>
        {threads?.map(item => {
          if (!item) {
            return;
          }
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate('Chat', {threadId: item._id})}>
              <Text>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default Threads;
