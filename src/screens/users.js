//import liraries
import React, {Component, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  allUser,
  currentUser,
  createNewThread,
  makeid,
  getThreadUser,
} from '../services/firebase';
import firestore from '@react-native-firebase/firestore';
// create a component
const Users = props => {
  const {navigation} = props;
  const [users, setUsers] = useState([]);
  const user = currentUser();
  useEffect(() => {
    const unsubscribe = allUser().onSnapshot(querySnapshot => {
      // console.log(querySnapshot.docs);
      const allThreads = querySnapshot.docs.map(snapshot => {
        return {
          ...snapshot.data(),
        };
      });

      setUsers(allThreads);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  console.log(users);

  const selectUser = async item => {
    await getThreadUser(item).then(async querySnapshot => {
      const allThreads = querySnapshot.map(snapshot => {
        return {
          _id: snapshot.id,
          ...snapshot.data(),
        };
      });
      if (allThreads.length < 1) {
        await createNewThread(item).then(() => selectUser(item));
      } else {
        const threadId = allThreads[0]?._id;
        navigation.navigate('Chat', {threadId: threadId});
      }
    });
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        {users.map(item => {
          if (user.uid === item.uid) {
            return null;
          }
          return (
            <TouchableOpacity
              onPress={() => selectUser(item)}
              key={item.uid}
              style={{marginVertical: 10}}>
              <Text>{item.email}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default Users;
