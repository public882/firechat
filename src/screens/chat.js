//import liraries
import React, {
  Component,
  useLayoutEffect,
  useCallback,
  useState,
  useReducer,
  useEffect,
} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import firestore from '@react-native-firebase/firestore';
import {
  listenToMessages,
  createMessage,
  currentUser,
  markThreadLastRead,
} from '../services/firebase';
// create a component
const Chat = props => {
  const {navigation} = props;
  const user = currentUser();
  const [messages, setMessages] = useState([]);
  const threadId = props?.route?.params?.threadId || null;

  useEffect(() => {
    const unsubscribe = listenToMessages(threadId).onSnapshot(querySnapshot => {
      const formattedMessages = querySnapshot.docs.map(doc => {
        return {
          _id: doc.id,
          text: '',
          createdAt: new Date().getTime(),
          user: {},
          ...doc.data(),
        };
      });
      setMessages(formattedMessages);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    return () => {
      if (messages.length > 0) {
        markThreadLastRead(user, threadId, new Date().getTime());
      }
    };
  }, [messages]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <GiftedChat
        messages={messages}
        onSend={newMessages => {
          const text = newMessages[0].text;
          createMessage(threadId, text);
        }}
        user={{
          _id: user.uid,
        }}
      />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default Chat;
