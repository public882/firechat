//import liraries
import React, {Component, useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';
import auth from '@react-native-firebase/auth';
import {signUpUser, loginUser, createUser} from '../services/firebase';
// import {auth} from '../services/firebase';
// create a component
const Login = props => {
  const {navigation} = props;
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  const onHandleLogin = () => {
    if (email !== '' && password !== '') {
      loginUser(email, password)
        .then(data => {
          console.log(data);
          createUser(data.user);
          navigation.navigate('Threads');
          console.log('User account created & signed in!');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
          }
          if (error.code === 'auth/user-not-found') {
            signUpUser(email, password).then(data => {
              onHandleLogin();
            });
          }

          console.error(error);
        });
    }
  };
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome back!</Text>
      <TextInput
        style={styles.input}
        placeholder="Enter email"
        autoCapitalize="none"
        keyboardType="email-address"
        textContentType="emailAddress"
        autoFocus={true}
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={styles.input}
        placeholder="Enter password"
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={true}
        textContentType="password"
        value={password}
        onChangeText={text => setPassword(text)}
      />
      <Button onPress={onHandleLogin} color="#f57c00" title="Login" />
      <Button
        onPress={() => navigation.navigate('Signup')}
        title="Go to Signup"
      />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 50,
    paddingHorizontal: 12,
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#444',
    alignSelf: 'center',
    paddingBottom: 24,
  },
  input: {
    backgroundColor: '#fff',
    marginBottom: 20,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 8,
    padding: 12,
  },
});

//make this component available to the app
export default Login;
