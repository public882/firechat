//import liraries
import React, {Component} from 'react';
import {LogBox} from 'react-native';
import Main from './navigation';
LogBox.ignoreAllLogs(true);
const App = () => {
  return <Main />;
};

export default App;
